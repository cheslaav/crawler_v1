defmodule CrawlerWeb.TenderControllerTest do
  use CrawlerWeb.ConnCase

  alias Crawler.Tenders

  @create_attrs %{city: "some city", contact_person: "some contact_person", email: "some email", fax: "some fax", keyword: "some keyword", org_name: "some org_name", phone: "some phone", site: "some site", status: "some status", tender_id: "some tender_id", tender_link: "some tender_link", tender_name: "some tender_name"}
  @update_attrs %{city: "some updated city", contact_person: "some updated contact_person", email: "some updated email", fax: "some updated fax", keyword: "some updated keyword", org_name: "some updated org_name", phone: "some updated phone", site: "some updated site", status: "some updated status", tender_id: "some updated tender_id", tender_link: "some updated tender_link", tender_name: "some updated tender_name"}
  @invalid_attrs %{city: nil, contact_person: nil, email: nil, fax: nil, keyword: nil, org_name: nil, phone: nil, site: nil, status: nil, tender_id: nil, tender_link: nil, tender_name: nil}

  def fixture(:tender) do
    {:ok, tender} = Tenders.create_tender(@create_attrs)
    tender
  end

  describe "index" do
    test "lists all tenders", %{conn: conn} do
      conn = get(conn, Routes.tender_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Tenders"
    end
  end

  describe "new tender" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.tender_path(conn, :new))
      assert html_response(conn, 200) =~ "New Tender"
    end
  end

  describe "create tender" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.tender_path(conn, :create), tender: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.tender_path(conn, :show, id)

      conn = get(conn, Routes.tender_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Tender"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.tender_path(conn, :create), tender: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Tender"
    end
  end

  describe "edit tender" do
    setup [:create_tender]

    test "renders form for editing chosen tender", %{conn: conn, tender: tender} do
      conn = get(conn, Routes.tender_path(conn, :edit, tender))
      assert html_response(conn, 200) =~ "Edit Tender"
    end
  end

  describe "update tender" do
    setup [:create_tender]

    test "redirects when data is valid", %{conn: conn, tender: tender} do
      conn = put(conn, Routes.tender_path(conn, :update, tender), tender: @update_attrs)
      assert redirected_to(conn) == Routes.tender_path(conn, :show, tender)

      conn = get(conn, Routes.tender_path(conn, :show, tender))
      assert html_response(conn, 200) =~ "some updated city"
    end

    test "renders errors when data is invalid", %{conn: conn, tender: tender} do
      conn = put(conn, Routes.tender_path(conn, :update, tender), tender: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Tender"
    end
  end

  describe "delete tender" do
    setup [:create_tender]

    test "deletes chosen tender", %{conn: conn, tender: tender} do
      conn = delete(conn, Routes.tender_path(conn, :delete, tender))
      assert redirected_to(conn) == Routes.tender_path(conn, :index)
      assert_error_sent 404, fn ->
        get(conn, Routes.tender_path(conn, :show, tender))
      end
    end
  end

  defp create_tender(_) do
    tender = fixture(:tender)
    %{tender: tender}
  end
end
