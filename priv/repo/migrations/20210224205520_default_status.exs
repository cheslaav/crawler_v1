defmodule Crawler.Repo.Migrations.DefaultStatus do
  use Ecto.Migration

  def change do
    alter table(:tenders) do
      modify :status, :string, default: "новый", null: false
    end
  end
end
