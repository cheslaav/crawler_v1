defmodule Crawler.Tenders.Tender do
  use Ecto.Schema
  import Ecto.Changeset

  schema "tenders" do
    field :city, :string
    field :contact_person, :string
    field :email, :string
    field :fax, :string
    field :keyword, :string
    field :org_name, :string
    field :phone, :string
    field :site, :string
    field :status, :string
    field :tender_id, :string
    field :tender_link, :string
    field :tender_name, :string

    timestamps()
  end

  @doc false
  def changeset(tender, attrs) do
    tender
    |> cast(attrs, [:tender_id, :org_name, :tender_name, :city, :site, :contact_person, :email, :phone, :fax, :keyword, :status, :tender_link])
    |> validate_required([:tender_id, :org_name, :tender_name, :city, :site, :contact_person, :email, :phone, :fax, :keyword, :tender_link])
  end
end
