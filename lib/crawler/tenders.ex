defmodule Crawler.Tenders do
  @moduledoc """
  The Tenders context.
  """

  import Ecto.Query, warn: false
  alias Crawler.Repo

  alias Crawler.Tenders.Tender


  @doc """
  Returns the list of tenders.

  ## Examples

      iex> list_tenders()
      [%Tender{}, ...]

  """

  def list_tenders(offset, limit, keyword) do
    query = from t in Tender,
    where: t.keyword in ^keyword and
    t.status != "deleted",
    offset: ^offset,
    limit: ^limit
    Repo.all(query)
  end

  def get_tenders_count(keyword) do
    # keyword = ["каталог", "НСИ"]
    query = from t in Tender,
    where: t.keyword in ^keyword and
    t.status != "deleted",
    select: %{
      record_count: count(t.id)
    }
    # |> where_id(filters[:id])
    # |> where_company_id(filters[:company_id])
    hd(Repo.all(query)).record_count
  end

  # def list_tenders(tender = %Tender{}) do
  #   Tender
  #   |> where(tender_id: ^tender.id)
  #   |> Repo.all()
  # end

  # def list_tenders(:paged, page, per_page) do
  #   Tender
  #   |> order_by(desc: ^tender.id)
  #   |> Crawler.Pagination.page(page, per_page: per_page)
  # end

  # def list_tenders(:paged, page, per_page) do
  #   Tenders
  #   |> order_by(desc: :id)
  #   |> Crawler.Pagination.page(page, per_page: per_page)
  # end

  # def list_tenders(tender = %Tender{}, page, per_page) do
  #   Tender
  #   |> where(tender_id: ^tender.id)
  #   |> order_by(desc: :id)
  #   |> Crawler.Pagination.page(page, per_page: per_page)
  # end

  @doc """
  Gets a single tender.

  Raises `Ecto.NoResultsError` if the Tender does not exist.

  ## Examples

      iex> get_tender!(123)
      %Tender{}

      iex> get_tender!(456)
      ** (Ecto.NoResultsError)

  """
  def get_tender!(id), do: Repo.get!(Tender, id)

  @doc """
  Creates a tender.

  ## Examples

      iex> create_tender(%{field: value})
      {:ok, %Tender{}}

      iex> create_tender(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_tender(attrs \\ %{}) do
    IO.inspect(attrs, label: "ATTRS!!!!!")
    %Tender{}
    |> Tender.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a tender.

  ## Examples

      iex> update_tender(tender, %{field: new_value})
      {:ok, %Tender{}}

      iex> update_tender(tender, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_tender(%Tender{} = tender, attrs) do
    tender
    |> Tender.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a tender.

  ## Examples

      iex> delete_tender(tender)
      {:ok, %Tender{}}

      iex> delete_tender(tender)
      {:error, %Ecto.Changeset{}}

  """
  def delete_tender(%Tender{} = tender) do
    Repo.delete(tender)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking tender changes.

  ## Examples

      iex> change_tender(tender)
      %Ecto.Changeset{data: %Tender{}}

  """
  def change_tender(%Tender{} = tender, attrs \\ %{}) do
    Tender.changeset(tender, attrs)
  end
end
