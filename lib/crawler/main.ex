defmodule Crawler.Main do


  def t() do
    keywords = ["каталог", "справочник", "нормализация", "классификация", "верификация", "нормативно", "НСИ", "МТР"]
    # Enum.reduce(filters, workbook, fn filter, workbook_acc -> Crawler.ZakupkiGov.make_report(filter, workbook_acc) end)
    Enum.map(keywords, fn keyword -> Crawler.ZakupkiGov.make_report(keyword) end)
  end

end
