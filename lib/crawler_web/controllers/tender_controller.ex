defmodule CrawlerWeb.TenderController do
  use CrawlerWeb, :controller

  alias Crawler.Tenders
  alias Crawler.Tenders.Tender

  @lines_per_page 5

  def index(conn, params) do

    page = try do
      max(1, String.to_integer(params["page"]))
    rescue
      _ -> 1
    end

    keyword = try do
      params["keyword"] || "справочник"
    rescue
      _ -> "справочник"
    end

    IO.inspect(keyword, label: "FETCH SOCKET!!!")
    total_lines = Tenders.get_tenders_count([keyword])
    max_page_cnt = div(total_lines, @lines_per_page) + (if rem(total_lines, @lines_per_page) == 0, do: 0, else: 1)
    page = max(1, min(max_page_cnt, page))
    offset = @lines_per_page * (page - 1)

    conn
    |> assign(:tenders, Tenders.list_tenders(offset, @lines_per_page, [keyword]))
    |> assign(:count, total_lines)
    |> assign(:page, page)
    |> assign(:max_page_cnt, max_page_cnt)
    |> assign(:keyword, keyword)
    |> render("index.html")
  end

  def new(conn, _params) do
    changeset = Tenders.change_tender(%Tender{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"tender" => tender_params}) do
    case Tenders.create_tender(tender_params) do
      {:ok, tender} ->
        conn
        |> put_flash(:info, "Процедура успешно создана")
        |> redirect(to: Routes.tender_path(conn, :show, tender))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    tender = Tenders.get_tender!(id)
    render(conn, "show.html", tender: tender)
  end

  def edit(conn, %{"id" => id}) do
    tender = Tenders.get_tender!(id)
    changeset = Tenders.change_tender(tender)
    render(conn, "edit.html", tender: tender, changeset: changeset)
  end

  def update(conn, %{"id" => id, "tender" => tender_params}) do
    tender = Tenders.get_tender!(id)

    case Tenders.update_tender(tender, tender_params) do
      {:ok, tender} ->
        conn
        |> put_flash(:info, "Процедура успешно обновлена")
        |> redirect(to: Routes.tender_path(conn, :show, tender))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", tender: tender, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id} = params) do
    tender = Tenders.get_tender!(id)
    Tenders.update_tender(tender, %{status: "deleted"})
    # tender = Tenders.get_tender!(id)
    # {:ok, _tender} = Tenders.delete_tender(tender)

    IO.inspect(params, label: "PARAMS!!!!")

    conn
    |> put_flash(:info, "Процедура успешно удалена")
    |> redirect(to: Routes.tender_path(conn, :index))
  #   page = try do
  #     max(1, String.to_integer(params["page"]))
  #   rescue
  #     _ -> 1
  #   end

  #   keyword = try do
  #     params["keyword"] || "каталог"
  #   rescue
  #     _ -> "каталог"
  #   end

  #   IO.inspect(keyword, label: "FETCH SOCKET!!!")
  #   total_lines = Tenders.get_tenders_count([keyword])
  #   max_page_cnt = div(total_lines, @lines_per_page) + (if rem(total_lines, @lines_per_page) == 0, do: 0, else: 1)
  #   page = max(1, min(max_page_cnt, page))
  #   offset = @lines_per_page * (page - 1)

  #   conn
  #   |> assign(:tenders, Tenders.list_tenders(offset, @lines_per_page, [keyword]))
  #   |> assign(:count, total_lines)
  #   |> assign(:page, page)
  #   |> assign(:max_page_cnt, max_page_cnt)
  #   |> assign(:keyword, keyword)
  #   |> render("index.html")
  end
end
