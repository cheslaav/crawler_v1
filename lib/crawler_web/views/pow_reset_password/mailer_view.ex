defmodule CrawlerWeb.PowResetPassword.MailerView do
  use CrawlerWeb, :mailer_view

  def subject(:reset_password, _assigns), do: "Reset password link"
end
